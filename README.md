# Yakforms.org

This is the source for [yakforms.org](https://yakforms.org), the official website for Yakforms.

This is a simple static website based on Pelican.

## How can I edit this website ?
You'll have to create a Merge Request on the git repo to propose your changes.

**If you don't know how to use Git**, you'll have to create an account on [Framagit](https://framagit.org). Once that is done, log in, browse [over here](https://framagit.org/theo1/yakforms_org/-/tree/master/content), locate the file you want to edit, edit it in the Web IDE. Check the "Start a MR with these changes" box and click "Commit changes".

**If you are comfortable with Git**, you can fork the repo, set the remote to your fork, and create a merge request with the changes you've made locally (instructions below).

## Run the site locally

First, you need to install Pelican and run the site locally.

1. Follow [the instructions](https://docs.getpelican.com/en/latest/install.html) to install Pelican.
2. Clone the repo : run `git clone https://framagit.org/theo1/yakforms_org.git` in the folder of your choice.
3. Browse to the folder, and run `pelican --autoreload --listen`. This will start a local server on your machine. You can now access the website on your browser, most likely at `localhost:8081`.

### Editing a page
Pelican translates Markdown into HTML, making it easy to edit the text content of the website.

Locate the page you want to edit under `content/pages` (`content/news` if you want to edit a news article), open it in the text editor of your liking, edit and hit save. Reload the page on your browser, and your changes should appear !

The only exception is for the index page : to display the frontpage blocks separately, each of the paragraphs lie in separate files.

### Translate a page

The site uses the [i18n module for Pelican](https://github.com/getpelican/pelican-plugins/tree/master/i18n_subsites/) to provide translated content.

Pelican looks for translated content under `content/pages/SaidPageName/[language code]` (`fr` for French, `de` for German, etc). To translate a page, just make a copy of the original page under the corresponding folder (create it if it doesn't exist). Change the `Lang` metadata at the top of the file to your language code, translate the title, and leave the rest unchanged.

You can now translate the content of the file.

### Page metadata

Each page has the following information at its top :
```
Title: My page title (will be displayed at the top of the page and in the tab)
Authors: me (doesn't matter, but Pelican needs it)
Date: 2020-06-14 (doesn't matter, but Pelican needs it)
Modified: 2020-06-14 (doesn't matter, but Pelican needs it)
Slug: my-page-title (must be lowercase with hyphens (-) between words : this is used for the URL)
Lang: fr (language code. Same as in the filename)
Nomenu: True (Insert this if you don't want the page to be listed in the navbar. Otherwise, delete this line.)
```

### Edit the theme
To edit the style of the website, you can edit the custom stylesheet `theme/pelican-clean-blog/static/css/custom.css`.

Watch out though : this will be applied everywhere, so you'll have to check you didn't break stuff on other pages.


## How can I add my instance to this site ?
If you have installed Yakforms and user subscriptions are open on your instance, then it can be added to the website for the world to see. To do so, please [open an issue](https://framagit.org/theo1/yakforms_org/-/issues/new) on the current git repo and provide the following information :
* the instance name
* its URL
* specify if the number of users subscribtion of forms per user is limited.

## Credits

This site is shipped with the following third-party content :
* Icons from [ForkAwesome](https://forkaweso.me/Fork-Awesome/)
* The title font is [Quando by Joana Correia da Silva](https://www.1001fonts.com/quando-font.html)
* The theme is adapted from [Pelican Clean Blog](https://github.com/gilsondev/pelican-clean-blog/tree/ea156f8f1741e473bc0ab848b7c8898112d6ffb5)

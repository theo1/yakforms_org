Title: Installer
Authors: moi
Date: 2020-06-14
Modified: 2020-06-14
Slug: install
Lang: fr
Weight: 2

_Ces instructions sont également disponibles [sur le wiki](https://framagit.org/yakforms/yakforms/-/wikis/Installing-Yakforms-through-the-installation-profile). Dernière mise à jour&nbsp;: 11 février 2021._

# Prérequis à l'installation

Avant de commencer, voici ce qu'il vous faudra sur votre système :

* **PHP >= 7.3**
  d'après les recommandations de Drupal 7, avec les modules suivants (téléchargeables avec la commande `sudo apt-get install php7.3-yourmodule` sur les systèmes basés sur Debian) :
    * `mb_string`
    * `pgsql`
    * `dom`
    * `gd`
    * `simplexml`
    * `xml`
    * `zip`
    *  `curl` est également nécessaire pour les sites de développement et de tests.
* **nginx.**
* **PostgreSQL 9.4.12 ou plus récent.**

# Configuration de nginx

Tout d'abord, configurez votre **reverse proxy** pour rendre Drupal accessible.

Voici une configuration nginx fonctionnelle :

```
server {
   listen 80;
   listen [::]:80;

   root /var/www/yakforms;
   index index.html;

   location ~ \.php$ {
      try_files $uri =404;
      include /etc/nginx/fastcgi_params;
      fastcgi_send_timeout 300s;
      fastcgi_read_timeout 300s;
      fastcgi_pass unix:/run/php/php7.3-fpm.sock;
      fastcgi_index index.php;
      fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
   }

   error_log /var/log/nginx/yakforms.error.log;
	access_log /var/log/nginx/yakforms.access.log combined;

    location / {
      autoindex on ;
      autoindex_exact_size off;
      try_files $uri index.html /index.php?$query_string;
    }
}
```

**Note : il vous faudra [ajouter votre configuration SSL](https://www.nginx.com/blog/using-free-ssltls-certificates-from-lets-encrypt-with-nginx/) à cette configuration nginx pour compléter cette étape.** Vous pouvez vérifier votre configuration avec la commande `nginx -t`.

C'est tout bon ? Alors vous pouvez passer à l'installation de Drupal !

# Étapes d'installation

1. **Téléchargez Yakforms.**
   Pour cela, suivez les instructions sur [la page dédiée]({filename}../Download/download-fr.md). Une fois extraits, placez les fichiers dans votre dossier accessible par le serveur web (`/var/www/yakforms` si vous avez utilisé le modèle NGINX ci-dessus).
2. **Créez votre base de données.**
   Les instructions sont disponibles en anglais sur [le site Drupal](https://www.drupal.org/docs/7/install/step-2-create-the-database).
3. **Créez votre fichier `settings.php`.**
   Les instructions sont sur [la documentation Drupal](https://www.drupal.org/docs/7/install/step-3-create-settingsphp-and-the-files-directory).
4. **Rendez-vous sur le script d'installation.**
   Naviguez sur votre site à l'adresse `youryakforms.ex/install.php `. Vous trouverez là les instructions pour mettre en place votre instance. Patientez pendant que tout s'installe, puis...
![Installation menu]({static}../../images/screenshots/install_installation_pannel.png)
1. **Renseignez les informations générales de votre site.**
   Vous devez indiquer le nom de votre site, une adresse mail de contact pour l'administrateur⋅ice, et des informations pour le compte d'administration. Vous utiliserez ce dernier pour la gestion du site, donc mémorisez bien le nom d'utilisateur et le mot de passe !
![Configuration pannel]({static}../../images/screenshots/install_config_pannel.png)
1. **Validez le formulaire.**
   Cela vous amènera sur la page d'accueil du site. Vous y êtes presque ! Plus que deux choses.
2. **Activez la _feature_ Yakforms[^2].**
   Cliquez sur « Modules » dans la barre d'administration en haut de page (ou rendez-vous directement sur `youryakforms.ex/admin/modules`), trouvez la section `yakforms_feature`, cochez la case correspondante et **enregistrez votre configuration** au moyen du bouton en bas de page.
![Activate feature menu]({static}../../images/screenshots/install_save_feature.png)
1. **Activez le _module_ Yakforms.**
   Sur le menu « Modules » toujours, activez le module **Yakforms** de la même manière. Ce module ne doit pas être activé tant que la _feature_ Yakforms ne l'est pas, ne brûlez pas les étapes.
2. **Activez les modules auxiliaires**.
   Yakforms vient avec deux modules qui apportent des fonctionnalités supplémentaires : Yakforms Share Results et Yakforms Public Results. Il est conseillé de les activer également. Plus d'information [ici](https://framagit.org/yakforms/yakforms/-/wikis/Module-:-yakforms_share_results) et [ici](https://framagit.org/yakforms/yakforms/-/wikis/Module-:-yakforms_public_results).
3. **Créez les pages par défaut (optionnel).**
   Yakforms fournit des modèles pour les pages d'accueil, les pages d'erreur 404/403, la documentation utilisateur, etc. Pour les générer, rendez-vous sur l'URL `youryakforms.ex/admin/config/system/yakforms` et cliquez sur le bouton "Créez les pages par défaut".

Votre site devrait maintenant être fonctionnel !

# Paramètres avancés (optionnels)

Comme Yakforms est basé sur Drupal 7, sa configuration est très facilement modifiable en accédant aux menus d'administration des différents modules.

Parmi les modifications que vous pourriez y apporter, il y a par exemple :

* **Cacher les erreurs.**
  Les erreurs du site, les avertissements et les messages sont par défaut visibles par tous les utilisateurs, mais vous souhaitez peut-être qu'ils ne le soient pas. Pour cela, naviguez à l'adresse `/admin/people/permissions`et désactivez les lignes correspondantes pour les utilisateur⋅ices authentifé⋅es et / ou les utilisateur⋅ices anonymes ("Voir les messages d'erreur", "Voir les messages d'avertissement").
* **Changer le répertoire de fichier privé.**
  Par défaut, les fichiers des utilisateur⋅ices qui ne doivent pas être accessibles publiquement sont stockés dans l'arborescence de fichier du site. Cela peut poser un problème de sécurité : il est conseillé de les placer dans un dossier qui n'est pas directement accessible par Internet.  Pour cela, créez un dossier sur votre serveur _en dehors_ de votre racine Yakforms (`/var/www/yakforms` ou autre), puis rendez-vous à l'adresse `/admin/config/media/file-system`et renseignez le chemin de ce dossier. Si vous obtenez une erreur, assurez-vous que ce dossier est accessible par l'utilisateur⋅ice du reverse-proxy (`www-data` pour nginx), et que ce dossier est accessible en écriture.
* **Modifier la langue de votre site.** Pour installer Yakforms dans une autre langue, [suivez ces instructions](https://framagit.org/yakforms/yakforms/-/wikis/Changing-an-instance-language#installing-another-language). ([Mais comment participer à la traduction de Yakforms ?]({filename}../Contribute/contribute-fr.md))


[^1]: Un profil d'installation Drupal permet simplement d'installer un groupe de module et de thèmes en même temps que Drupal afin de donner tout de suite accès à certains fonctionnalités. Il contient un script d'installation qui s'exécute à l'installation de Drupal, ainsi que toutes les dépendances nécessaires (modules, thèmes). Dans notre cas, nous utilisons le profil `yakforms_org` pour embarquer tous les modules Drupal nécessaires (notamment les modules `Webform` et  `Form builder`, mais pas que !), ainsi que des thèmes et du contenu statique (images, modèles HTML...)

[^2]: Une `feature` Drupal est une manière de rapidement configurer un site (et tous ses modules) avec des valeurs par défaut. La configuration de Drupal se trouve dans sa base de données, ce qui veut dire qu'avec un nouveau site, il faudrait recommencer toute la configuration. Mais à l'aide d'une `feature`, plus besoin. Yakforms fonctionne avec une unique `feature`, nommée sobrement `yakforms_feature`, qui contient toute la configuration nécessaire pour Yakforms : les vues qui permettent de visualiser des listes de contenu, les permissions associées aux différents rôles...

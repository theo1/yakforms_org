Title: Bienvenue
Authors: moi
Date: 2020-06-14
Modified: 2020-06-14
Status: hidden
Slug: index-par-5
Lang: en
BTarget: ./pages/contribute.html
BText: Contribute to Yakforms
BIcon: user-plus


# $$users$$ Contributing to Yakforms

Yakforms is a free libre software, distributed under GPL 2.0. This means anyone can contribute to the software's evolution, with or without technical knowledge.

By joining the Yakforms community, you help making the software better : maintain the code, designing how the user will interact with the service, translate it, document it...

If you'd like to know more about the community's Code of Conduct, chat with the people involved or find out more about the source code :

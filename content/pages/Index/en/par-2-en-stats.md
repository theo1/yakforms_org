Title: Bienvenue
Authors: moi
Date: 2020-06-14
Modified: 2020-06-14
Status: hidden
Slug: index-par-2
Id: frontpage-stats
Lang: en


# Yakforms in numbers

<span class='stats-number'>3</span> <span class='stats-label'>instances</span>,

<span class='stats-number'>7 000</span> <span class='stats-label'>users creating forms every day</span>,

<span class='stats-number'>825 000</span> <span class='stats-label'>forms since launch</span>

<span class='stats-number'>18 000 000</span> <span class='stats-label'>submissions (still growing !)</span>

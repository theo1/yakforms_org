Title: Bienvenue
Authors: moi
Date: 2020-06-14
Modified: 2020-06-14
Status: hidden
Slug: index-par-3
Lang: en
BTarget: ./pages/install.html
BText: Install Yakforms
BIcon: rocket


# $$step-forward$$ Hosting a form service

Yakforms is a SaaS software that you can host on your organization's server.

Hosting your own instance of Yakforms lets you take full control over your data, and helps building a decentralized web. Yakforms will easily fit your user's needs.

Title: Bienvenue
Authors: moi
Date: 2020-06-14
Modified: 2020-06-14
Status: hidden
Slug: index-par-1
Lang: en
BTarget: ./pages/getting-started.html
BText: Explore Yakforms
BIcon: plane

# $$list-ul$$ Quickly build forms for your community

Yakforms is a free-as-in-free-speech software that allows you to easily create forms for your community. Whether you need a full-blown or a quick poll, Yakforms will fit your needs.

<div markdown class='features-tabs'role="tablist">
<div>
  <a role='tab' href='#1' class='tab-active'>Build</a>
  <a role='tab' href='#2'>Customize</a>
  <a role='tab' href='#3'>Share</a>
  <a role='tab' href='#4'>Analyze</a>
</div>

![You can create forms easily through a drag-and-drop interface.]({static}../../../images/frontpage_tabs/01-edit.png)
**Create forms easily.**
Edit your forms by a simple drag-and-drop. The large number of available form fields will cover all your needs.

![Pick from many themes to make your forms look great.]({static}../../../images/frontpage_tabs/05-themes.png)
**Forms that look just like you.**
Yakforms come with many themes that you can pick from when you create a form. There is plenty : you'll find one for your needs.

![Partagez facilement vos formulaires via lien, ou intégrez-les sur votre site.]({static}../../../images/frontpage_tabs/03-share.png)
**Share your forms.**
Once your form is finished, share it by email or on social media. You can even embed it in your own website and allow others to clone it to save precious time.

![Procédez à une analyse rapide de vos données sur le site, ou téléchargez vos réponses au format excel.]({static}../../../images/frontpage_tabs/04-analyze.png)
**Make sense of your results.**
You can access your submissions online and visualize them as graphs, or export everything to a spreadsheet.
</div>

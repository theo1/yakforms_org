Title: Bienvenue
Authors: moi
Date: 2020-06-14
Modified: 2020-06-14
Status: hidden
Slug: index-par-4
Id: they-installed
Lang: en

# They installed Yakforms

[![Logo Framasoft]({static}../../../images/they-installed/framasoft.png)](https://framasoft.org/fr) [![Logo Ministère des Affaires Étrangère]({static}../../../images/they-installed/logo_ambassade_france_maroc.jpg)](https://ma.ambafrance.org/)

Title: Bienvenue
Authors: moi
Date: 2020-06-14
Modified: 2020-06-14
Status: hidden
Slug: index-par-3
Lang: fr
BTarget: ./pages/install.html
BText: Installer Yakforms
BIcon: rocket

# $$step-forward$$ Hébergez votre propre outil de formulaire

Yakforms est un logiciel (en mode <abbr title="Software As A Service">SaaS</abbr>) que vous pouvez installer sur le serveur de votre administration, entreprise ou autre organisation.

En hébergeant une instance de Yakforms, vous maîtrisez totalement vos données et vos usages et vous participez à la décentralisation des services sur Internet. Yakforms s'adapte facilement à vos propres besoins.

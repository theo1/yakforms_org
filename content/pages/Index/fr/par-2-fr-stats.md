Title: Bienvenue
Authors: moi
Date: 2020-06-14
Modified: 2020-06-14
Status: hidden
Slug: index-par-2
Id: frontpage-stats
Lang: fr

# Yakforms, c'est

<span class='stats-number'>3</span> <span class='stats-label'>instances</span>,

<span class='stats-number'>7 000</span> <span class='stats-label'>internautes qui créent des formulaires chaque jour</span>

<span class='stats-number'>825 000</span> <span class='stats-label'>formulaires créés depuis la nuit des temps</span>

<span class='stats-number'>18 millions</span> <span class='stats-label'>de réponses (et ça monte encore !)</span>

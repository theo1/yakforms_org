Title: Legals
Authors: moi
Date: 2020-06-14
Modified: 2020-06-14
Status: hidden
Slug: legals
Lang: en

This page has not yet been translated. Please see [the French version]({filename}./legals-fr.md), for now.

If you want to opt-out from our tool for visits’ statistics:

<iframe
    title="Matomo"
    style="border: 0; height: 200px; max-width: 600px; width: 85%;"
    src="https://stats.framasoft.org/?module=CoreAdminHome&action=optOut&language=en"
></iframe>

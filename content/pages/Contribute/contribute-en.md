Title: Contribute
Authors: moi
Date: 2020-06-14
Modified: 2020-06-14
Slug: contribute
Lang: en
Weight: 3


Yakforms is **free software**, open to contribution and suggestion.

To do that, we share thoughts on the [**Framacolibri section**](https://framacolibri.org/c/yakforms/54), be it enhancement, bug reports, ideas for new features, mutual help using the software, etc.

Anyone is welcome to open a topic.

# $$lightbulb-o$$ Contribute without coding

Here is a list of tasks that are very helpful and don't necessitate technical knowledge :

<div class='links-group'>
<div class='links-group-el'>
  <a href="https://weblate.framasoft.org/engage/framaforms/">
    <span>
    <i class="fa fa-language fa-2x" aria-hidden="true"></i>
    </span>
    <span>
    <div class='links-group-subtitle'>Translate</div>
    <div class='links-group-description'>the software with Weblate</div>
    </span>
  </a>
</div>
<div class='links-group-el'>
  <a href="https://framagit.org/yakforms/yakforms-docs">
    <span>
    <i class="fa fa-book fa-2x" aria-hidden="true"></i>
    </span>
    <span>
    <div class='links-group-subtitle'>Improve</div>
    <div class='links-group-description'>the software's documentation</div>
    </span>
  </a>
</div>
<div class='links-group-el'>
  <a href="https://framacolibri.org/c/yakforms/">
    <span>
    <i class="fa fa-comments fa-2x" aria-hidden="true"></i>
    </span>
    <span>
    <div class='links-group-subtitle'>Improve</div>
    <div class='links-group-description'>the software's documentation</div>
    </span>
  </a>
</div>
<div class='links-group-el'>
  <a href="https://framacolibri.org/c/yakforms/54">
    <span>
    <i class="fa fa-hand-pointer-o fa-2x" aria-hidden="true"></i>
    </span>
    <span>
    <div class='links-group-subtitle'>Design</div>
    <div class='links-group-description'>the software's interface
    </div>
    </span>
  </a>
</div>
</div>

# $$terminal$$ Contributing code

If you have **technical knowledge**, you can also contribute to the code.

Yakforms makes use of [Drupal 7](https://www.drupal.org/drupal-7.0), the famous tried-and-tested open-source <abbr title="Content Management System">CMS</abbr>, using a lot of its core feature and modules, along with custom features. Contributing to Yakforms' code means developping Drupal 7 modules.

Here's a few ways you can help :

* **Working on an open issue** : for your first contribution to Yakforms, you can start by tackling `🤏 easy`-labelled issues, which are very helpful and perfect to get started. You can also participate to the open discussions below issues, all opinions are welcome.
* **Writing tests** : as a Drupal-based site, Yakforms can make use of the [native function testing framework](https://www.drupal.org/docs/7/testing/simpletest-testing-tutorial-drupal-7). Developing those tests allow for a more rigorous development process. Try to isolate one feature and create a test around it.

If something seems to be missing on this site, you can also edit it [here](https://framagit.org/yakforms/yakforms_org).

# $$fa fa-hand-spock-o$$ Code of Conduct

All contribution spaces should be inclusive, open and welcoming to everyone. We ask of anyone wanting to contribute to Yakforms to follow these guidelines :

* Remain patient and polite.
* Oppose and report any form of discrimination against minorities, be it racist, sexist, homophobic, ableist or other.
* Avoid "techbro"-like postures : having technical abilities doesn't mean you're right about everything, nor that you should be the one to decide on every topic.

_The community around Yakforms is still recent, and the code of conduct has to be discussed. Join the discussion by opening a topic in [the forum](https://framacolibri.org/c/yakforms/54)._

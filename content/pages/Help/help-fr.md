Title: Aide
Authors: moi
Date: 2020-06-14
Modified: 2020-06-14
Slug: help
Lang: fr
Weight: 4

# Mieux comprendre Yakforms

95% des questions trouvent leurs réponses dans [notre FAQ](https://docs.yakforms.org/faq/).

Notre [documentation](https://docs.yakforms.org/) détaille toutes les fonctionnalités de Yakforms.

# Découvrir la communauté Yakforms

Si une question vous taraude ou que vous souhaitez en savoir plus sur Yakforms, n'hésitez pas à ouvrir un sujet sur [le forum](https://framacolibri.org/c/yakforms/54).

# Installer et administrer Yakforms

Les administrateur⋅ices d'instances trouveront des instructions pour :

* [Installer Yakforms]({filename}../Install/install-fr.md).
* [Configurer son instance](https://framagit.org/yakforms/yakforms/-/wikis/Configuring-Yakforms).
* [Ajouter des modules à son instance](https://framagit.org/yakforms/yakforms/-/wikis/Enhancing-Yakforms).

# Contribuer à Yakforms

Si vous souhaitez participer à la construction de Yakforms, merci de vous référer au [guide de contribution]({filename}../Contribute/contribute-fr.md).

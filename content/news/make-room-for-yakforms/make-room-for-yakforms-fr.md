Title: Oubliez Framaforms, faites de la place à Yakforms !
Authors: moi
Date: 2021-05-25
Modified: 2020-05-25
Slug: make-room-for-yakforms
Category: News
Summary: Après 5 ans d'existence, le logiciel Framaforms fait peau neuve et devient Yakforms.
Lang: fr

En 2016, l'association [Framasoft](https://framasoft.org/fr/) ouvre les portes du service Framaforms, une alternative à Google Forms. Le logiciel du même nom qui sert à proposer ce service, libre et placé sous **licence AGPLv2**, fait alors partie de la campagne [Degooglisons Internet](https://degooglisons-internet.org/fr/), visant à montrer qu'il existe des alternatives éthiques aux services centralisant nos données chez les GAFAM.

Après avoir maintenu ce logiciel pendant 5 ans, l'association Framasoft a décidé de le rendre à sa (vaste) communauté. Et voilà, c'est fait : Framaforms s'est affûté les cornes, s'est laissé pousser les poils et est devenu Yakforms. Souhaitons-lui la bienvenue !

Retrouvez toute l'histoire sur [le Framablog »](https://framablog.org/2021/05/25/oubliez-framaforms-le-logiciel-faites-de-la-place-a-yakforms/)

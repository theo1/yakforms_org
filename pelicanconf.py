#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals
import re
import json

AUTHOR = 'me'
SITENAME = 'Yakforms'
SITESUBTITLE = "Free software to take back control of your forms"
SITEURL = ''
RELATIVE_URLS = False
DISPLAY_CATEGORIES_ON_MENU = True

PATH = 'content'
OUTPUT_PATH = 'public'

THEME = 'theme/pelican-clean-blog'
THEME_STATIC_DIR = 'theme'

FAVICON = 'images/logo/icone_yakforms.png'

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = 'fr'
DATE_FORMATS = {
    'en': '%m/%d/%Y',
    'fr': '%d/%m/%Y',
}
DEFAULT_DATE_FORMAT = '%a %d %B %Y'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

DEFAULT_ORPHANS = 0
PAGINATED_TEMPLATES = {'category': 5}
DEFAULT_PAGINATION = 5

PLUGIN_PATHS = ['./plugins']
PLUGINS = ['i18n_subsites', 'forkawesome_integration']

I18N_SUBSITES = {
    'en': {
        'SITENAME': 'Yakforms',
    },
  }

I18N_UNTRANSLATED_ARTICLES = 'hide'

# Load translation from json file
i18n_file = open('i18n.json')
I18N = json.load(i18n_file)

PAGES_TITLE_ICONS = {
    'explore': 'fa-list-ul',
    'install': 'fa-step-forward',
    'contribute': 'fa-users'
}


# Custom Jinja2 filter to get the Index page from Markdown
# From https://siongui.github.io/2016/02/19/pelican-generate-index-html-by-rst-or-md/
def hidden_pages_get_page_with_slug_index(hidden_pages):
    for page in hidden_pages:
        if page.slug == "index":
            return page

def index_paragraphs(hidden_pages):
    pars = []
    for page in hidden_pages:
        if re.match("^index-par-.$", page.slug):
            pars.append(page)
        # Insert alerts at the top of the page
        if re.match('^index-alert$', page.slug):
            pars.insert(0, page)
    return pars

def order_menu_by_weight(pages):
    # Filter out pages without "weight" attribute
    weighted = []
    for page in pages:
        try :
            if page.weight:
                weighted.append(page)
        except:
            pass
    return sorted(weighted, key=lambda x: x.weight)

# Returns page's associated icon for display on menu
def get_menu_link_icon(slug):
    return PAGES_TITLE_ICONS.get(slug)

# Based on the i18n.json file, returns translation of passed word.
def translate(word, lang='en'):
    if lang == 'en':
        return word
    return I18N[lang][word]

def print_machine(articles):
    print(repr(articles))
    for article in articles:
        print(">>>>>")
        print(article.title)

JINJA_FILTERS = {
    "hidden_pages_get_page_with_slug_index":
    hidden_pages_get_page_with_slug_index,
    "index_paragraphs": index_paragraphs,
    "order_menu_by_weight": order_menu_by_weight,
    "prepare_icon": get_menu_link_icon,
    'translate': translate,
    'print_machine': print_machine
}
